import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormArray } from '@angular/forms';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor() { }
  loadData: boolean = false; /*for loading */
  form_userInfo = new FormGroup({
    name: new FormControl(''),
    age: new FormControl(''),
    group: new FormArray([]),
  })
  /* data for updatinf form */
  dataForUpdateTest = {
    name: 'Ahmed Aladdin',
    age: '27 years old',
    group: [
      {
        groupName: 'development Track',
        courses: [
          { courseName: 'Angular' },
          { courseName: 'Ionic' }
        ]
      },
      {
        groupName: 'Programing',
        courses: [
          { courseName: 'OOP' },
          { courseName: 'python' },
          { courseName: 'typeScript' }
        ]
      }
    ]
  }
  ngOnInit() {
  }
  /* get Form Arrays from the form group (group and courses)*/
  getGroupControl() {
    return this.form_userInfo.get('group') as FormArray;
  }

  getCourseControl(groupIndex): FormArray {
    return this.getGroupControl().at(groupIndex).get('courses') as FormArray
  }
  /* this function  add groups in the form hen click on btn add group*/
  addGroup() {
    this.getGroupControl().push(new FormGroup({
      groupName: new FormControl(''),
      courses: new FormArray([]),
    }));
  }
  /* this function  add course in its owen group in the form  when click on btn add cousre  */
  addCourse(groupIndex) {
    this.getCourseControl(groupIndex).push(new FormGroup({
      courseName: new FormControl(''),
    }));
    console.log(this.getCourseControl(groupIndex));
  }
  /*  this function delete groups from the form  when click on btn delete group */
  deleteGroup(groupIndex) {
    console.log(groupIndex);
    this.getGroupControl().removeAt(groupIndex)
  }
  /* delete course from its owen group in  the form  when click on btn delete course */
  deleteCourse(groupIndex, courseIndex) {
    console.log('course', courseIndex);
    console.log('group', groupIndex);
    this.getCourseControl(groupIndex).removeAt(courseIndex)
  }

  /* save the data of the form  display it in consle log  (POST this data to the Server)*/
  onSubmit() {
    console.log('value is', this.form_userInfo.value);
  }
  /* get  a const data for test  after 2 second just for (Simulation Reality => patch this data to the Server)*/
  updateForm() {
    this.loadData = true;
    setTimeout(() => {
      for (let i = 0; i < this.dataForUpdateTest.group.length; i++) {
        this.addGroup();
        for (const course of this.dataForUpdateTest.group[i].courses) {
          this.addCourse(i);
        }
        this.form_userInfo.patchValue(this.dataForUpdateTest);
        this.loadData = false;
      }
    }, 2000);
  }
}
